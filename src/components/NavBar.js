import React, { Component } from 'react';
import NavItem from './NavItem';

class Navbar extends Component {

    render() {
        return (
            <nav>
                <img></img>
                <ul>
                    <NavItem item="Home" tolink="/"></NavItem>
                    <NavItem item="Personnel" toLink="/"></NavItem>
                    <NavItem item="Daily" toLink="/"></NavItem>
                </ul>
            </nav>
        )
    }
}

export default Navbar
